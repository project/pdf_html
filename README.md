INTRODUCTION
------------

Simple module that provides integration with pdftohtml 
php library (https://packagist.org/packages/gufy/pdftohtml-php).
Generates nodes from the given PDF documents and stores 
generated HTML as a cotent for target textarea field.
Optionally you can store the pdf document in the target Content type file field.

REQUIREMENTS
------------

Requires Poppler-utils to be installed on the server.
See instructions here https://packagist.org/packages/gufy/pdftohtml-php

INSTALLATION 
------------

Install with composer require to bring php library dependencies.
Install Poppler-utils.

CONFIGURATION
------------

1. Set Poppler utils install path on the form admin/config/development/pdf-html
2. Enter PDF files folder location.
3. Choose Target Content Type
4. Choose target field where to store generated HTML
5. Optionally set file field to store processed PDF file.
