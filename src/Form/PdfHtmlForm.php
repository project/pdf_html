<?php

namespace Drupal\pdf_html\Form;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Batch\BatchBuilder;
use Gufy\PdfToHtml\Config;
use Gufy\PdfToHtml\Pdf;
use Gufy\PdfToHtml\Base;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Pelago\Emogrifier;
use Drupal\node\Entity\Node;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\Render\Renderer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @file
 * Contains \Drupal\pdf_html\Form\PdfHtmlForm.
 */

/**
 * PDF to HTML generator form.
 */
class PdfHtmlForm extends FormBase {
  /**
   * Batch Builder.
   *
   * @var \Drupal\Core\Batch\BatchBuilder
   */
  protected $batchBuilder;
  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;
  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;
  /**
   * Entity Stream Wrapper.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManager
   */
  protected $streamWrapperManager;
  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;
  /**
   * Drupal Renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * BatchForm constructor.
   */
  public function __construct(ConfigFactory $configFactory, EntityFieldManager $entityFieldManager, StreamWrapperManager $streamWrapperManager, EntityTypeManager $entityTypeManager, Renderer $renderer) {
    $this->batchBuilder = new BatchBuilder();
    $this->streamWrapperManager = $streamWrapperManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->renderer = $renderer;
    $this->config = $configFactory->getEditable('pdf_html.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('config.factory'),
      $container->get('entity_field.manager'),
      $container->get('stream_wrapper_manager'),
      $container->get('entity_type.manager'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pdf_html_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $options = [];
    $options_fields = [];
    $options_file_fields['_none'] = '- Select -';
    if (!empty($form_state->getValue('node_types'))) {
      $bundle = $form_state->getValue('node_types');
      $fields = $this->entityFieldManager
        ->getFieldDefinitions('node', $bundle);
      foreach ($fields as $field) {
        switch ($field->getType()) {
          case 'text_long':
          case 'text_with_summary':
          case 'string_long':
            // Ignore revision_log field.
            if ($field->getName() != 'revision_log') {
              $options_fields[$field->getName()] = $field->getLabel();
            }
            break;

          case 'file':
            $options_file_fields[$field->getName()] = $field->getLabel();
            break;
        }
      }
    }
    $node_types = $this->entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();
    foreach ($node_types as $node_type) {
      $options[$node_type->id()] = $node_type->label();
    }
    $form['poppler_location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location of poppler-utils package'),
      '#description' => $this->t('Example /usr/local/bin on linux, <br> C:/poppler-0.68.0/bin on Windows, <br> /usr/local/bin on MacOS'),
      '#size' => 60,
      '#default_value' => $this->config->get('pdf_html_poppler_location'),
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $form['destination'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location of PDF files'),
      '#description' => $this->t('Example sites/default/files/pdfs'),
      '#size' => 60,
      '#default_value' => $this->config->get('pdf_html_destination'),
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $form['node_types'] = [
      '#title' => $this->t('Destination content type'),
      '#type' => 'select',
      '#description' => $this->t('Select the destination content type where the new content will be generated.'),
      '#options' => $options,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::pdfHtmlUpdateFieldValues',
        'wrapper' => 'update-fields',
        'event' => 'change',
      ],
    ];
    $form['node_field_types'] = [
      '#title' => $this->t('Destination content type textarea fields'),
      '#type' => 'select',
      '#description' => $this->t('Select the destination field where to generate and store html, Options will appear once you select destination content type'),
      '#options' => $options_fields,
      '#required' => TRUE,
      '#prefix' => "<div id='update-fields'>",
      '#suffix' => "</div>",
    ];
    $form['node_field_file_types'] = [
      '#title' => $this->t('Destination content type file fields'),
      '#type' => 'select',
      '#description' => $this->t('Select the destination field where to generate and store html, Options will appear once you select destination content type'),
      '#options' => $options_file_fields,
      '#prefix' => "<div id='update-file-fields'>",
      '#suffix' => "</div>",
    ];
    $form['node_published'] = [
      '#title' => $this->t('Published'),
      '#type' => 'checkbox',
      '#default_value' => $this->config->get('node_published'),
      '#description' => $this->t('Uncheck this field if you want generated content to be unpublished.'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Start',
      '#attributes' => [
        'class' => ['button--primary'],
      ],
    ];
    return $form;
  }

  /**
   * AJAX callback.
   */
  public function pdfHtmlUpdateFieldValues(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#update-fields', $this->renderer->render($form['node_field_types'])));
    $response->addCommand(new ReplaceCommand('#update-file-fields', $this->renderer->render($form['node_field_file_types'])));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (substr($form_state->getValue('poppler_location'), -1) == '/') {
      $form_state->setErrorByName('poppler_location', $this->t('Please remove the trailing slash from "Location of poppler-utils package"'));
    }
    if (!empty($form_state->getValue('destination'))) {
      // Get random file path form the directory.
      $files = array_diff(preg_grep('~\.(pdf)$~', scandir($form_state->getValue('destination'))), [
        '.',
        '..',
      ]);
      if (!empty($files)) {
        // Check environment.
        $windows = '';
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
          $windows = '.exe';
        }
        // Check if poppler exists and can do generation.
        Config::set('pdfinfo.bin', $form_state->getValue('poppler_location') . '/pdfinfo' . $windows);
        Config::set('pdftohtml.bin', $form_state->getValue('poppler_location') . '/pdftohtml' . $windows);
        $file_path = $form_state->getValue('destination') . '/' . $files[array_rand($files)];
        $info = new Pdf($file_path);
        $pdf_info = $info->getInfo();
        if (empty($pdf_info)) {
          $form_state->setErrorByName('poppler_location', $this->t('Poppler location is not properly configured.'));
        }
      }
      else {
        $form_state->setErrorByName('destination', $this->t('No PDF files found in the destination folder.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Submit configuration.
    $this->config->set('pdf_html_poppler_location', $form_state->getValue('poppler_location'));
    $this->config->set('pdf_html_destination', $form_state->getValue('destination'));
    $this->config->set('node_published', $form_state->getValue('node_published'));
    $this->config->save();
    $target_bundle = $form_state->getValue('node_types');
    $target_bundle_field = $form_state->getValue('node_field_types');
    $target_bundle_file_field = $form_state->getValue('node_field_file_types');
    $this->batchBuilder
      ->setTitle($this->t('Processing generation of nodes'))
      ->setInitMessage($this->t('Initializing.'))
      ->setProgressMessage($this->t('Completed @current of @total.'))
      ->setErrorMessage($this->t('An error has occurred.'));
    $items = [];
    $this->batchBuilder->setFile(drupal_get_path('module', 'pdf_html') . '/src/Form/PdfHtmlForm.php');
    $files = array_diff(preg_grep('~\.(pdf)$~', scandir($form_state->getValue('destination'))), [
      '.',
      '..',
    ]);
    foreach ($files as $file) {
      $file_path = $form_state->getValue('destination') . '/' . $file;
      $items[] = [$file => $file_path];
    }
    $this->batchBuilder->addOperation([$this, 'processItems'], [
      $items,
      $target_bundle,
      $target_bundle_field,
      $target_bundle_file_field,
    ]);
    $this->batchBuilder->setFinishCallback([$this, 'finished']);
    batch_set($this->batchBuilder->toArray());
  }

  /**
   * Processor for batch operations.
   */
  public function processItems($items, $target_bundle, $target_bundle_field, $target_bundle_file_field, array &$context) {
    // Elements per operation.
    $limit = 50;
    // Set default progress values.
    if (empty($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($items);
    }
    // Save items to array which will be changed during processing.
    if (empty($context['sandbox']['items'])) {
      $context['sandbox']['items'] = $items;
    }
    $counter = 0;
    if (!empty($context['sandbox']['items'])) {
      // Remove already processed items.
      if ($context['sandbox']['progress'] != 0) {
        array_splice($context['sandbox']['items'], 0, $limit);
      }
      foreach ($context['sandbox']['items'] as $item) {
        if ($counter != $limit) {
          $file_name_tmp = array_keys($item);
          $file_path_tmp = array_values($item);
          $file_name = $file_name_tmp['0'];
          $file_path = $file_path_tmp['0'];
          $this->processItem($file_name, $file_path, $target_bundle, $target_bundle_field, $target_bundle_file_field);
          $counter++;
          $context['sandbox']['progress']++;
          $context['message'] = $this->t('Now processing file :progress of :count', [
            ':progress' => $context['sandbox']['progress'],
            ':count' => $context['sandbox']['max'],
          ]);
          // Increment total processed item values. Will be used in finished
          // callback.
          $context['results']['processed'] = $context['sandbox']['progress'];
        }
      }
    }
    // If not finished all tasks, we count percentage of process. 1 = 100%.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Process single item.
   */
  public function processItem($file_name, $file_path, $target_bundle, $target_bundle_field, $target_bundle_file_field) {
    $windows = '';
    $public_holder = $this->streamWrapperManager
      ->getViaScheme(\Drupal::config('system.file')->get('default_scheme'));;
    $public_dir = $public_holder->getDirectoryPath();
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
      $windows = '.exe';
    }
    // Set pdfinfo bin location.
    Config::set('pdftohtml.bin', $this->config->get('pdf_html_poppler_location') . '/pdftohtml' . $windows);
    Config::set('pdfinfo.bin', $this->config->get('pdf_html_poppler_location') . '/pdfinfo' . $windows);
    $info = new Pdf($file_path);
    $pdf = new Base($file_path, ['singlePage' => TRUE, 'noFrames' => FALSE]);
    $random_dir = uniqid();
    $output_dir = Config::get('pdftohtml.output', $public_dir . '/pdf-html/' . $random_dir);
    if (!file_exists($output_dir)) {
      mkdir($output_dir, 0777, TRUE);
    }
    $pdf->setOutputDirectory($output_dir);
    $pdf->generate();
    $pdf_info = $info->getInfo();
    $pages = $info->getPages();
    $fileinfo = pathinfo($file_path);
    $base_path = $pdf->outputDir . '/' . $fileinfo['filename'];
    $contents = [];
    for ($i = 1; $i <= $pages; $i++) {
      $content = file_get_contents($base_path . '-' . $i . '.html');
      $content = str_replace($public_dir . '/pdf-html', '/' . $public_dir . '/pdf-html', $content);
      $parser = new Emogrifier($content);
      $content = $parser->emogrifyBodyContent();
      file_put_contents($base_path . '-' . $i . '.html', $content);
      $contents[$i] = file_get_contents($base_path . '-' . $i . '.html');
    }
    $html = '';
    foreach ($contents as $content) {
      $html .= $content;
    }
    // Create file object from remote URL.
    $data = file_get_contents($file_path);
    $file = file_save_data($data, 'public://' . $file_name, FileSystemInterface::EXISTS_REPLACE);
    // Create node object with attached file.
    $node = Node::create([
      'type' => $target_bundle,
      'title' => isset($pdf_info['subject']) ? $pdf_info['subject'] : str_replace('.pdf', '', $file_name),
      $target_bundle_field => [
        'value' => $html,
        'format' => 'full_html',
      ],
      $target_bundle_file_field => [
        'target_id' => $file->id(),
        'alt' => isset($pdf_info['subject']) ? $pdf_info['subject'] : str_replace('.pdf', '', $file_name),
        'title' => isset($pdf_info['subject']) ? $pdf_info['subject'] : str_replace('.pdf', '', $file_name),
      ],
      'status' => $this->config->get('node_published'),
    ]);
    $node->save();
  }

  /**
   * Finished callback for batch.
   */
  public function finished($success, $results, $operations) {
    $message = $this->t('Number of nodes created: @count', [
      '@count' => $results['processed'],
    ]);
    $this->messenger()
      ->addStatus($message);
  }

}
